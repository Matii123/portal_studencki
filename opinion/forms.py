"""Forms file."""
from django import forms
from .models import Opinion


class OpinionForm(forms.ModelForm): # noqa D101
    class Meta: # noqa D106
        model = Opinion
        fields = ['content', 'fields']

    def __init__(self, *args, **kwargs): # noqa D107
        super(OpinionForm, self).__init__(*args, **kwargs)
        self.fields['content'].widget.attrs.update({'class': 'form-control'})
        self.fields['fields'].widget.attrs.update({'class': 'form-control'})
