"""Models file."""
from django.db import models
from django.shortcuts import reverse

from accounts.models import CustomUser


class FieldOfStudy(models.Model): # noqa D101
    name = models.CharField(max_length=40, unique=True, blank=False)
    slug = models.SlugField(max_length=70, unique=True, blank=False)

    class Meta: # noqa D106
        verbose_name_plural = 'Field of studies'

    def __str__(self): # noqa D105
        return self.name

    def get_url(self): # noqa D102
        return reverse('opinions', kwargs={
            'slug': self.slug,
        })

    @property
    def num_opinions(self): # noqa D102
        return Opinion.objects.filter(fields=self).count()

    @property
    def last_opinion(self): # noqa D102
        return Opinion.objects.filter(fields=self).latest('date')

class Opinion(models.Model): # noqa D101
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, blank=True)
    fields = models.ForeignKey(FieldOfStudy, on_delete=models.CASCADE, blank=False, null=False)
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self): # noqa D105
        return self.content[:50]
