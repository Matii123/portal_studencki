"""Urls file."""
from django.urls import path
from .views import FieldsView, OpinionsView, CreateOpinionView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('opinie/kierunki/', FieldsView.as_view(), name='fields'),
    path('opinie/kierunek/<slug>/', OpinionsView.as_view(), name='opinions'),
    path('opinie/dodaj_opinie', CreateOpinionView.as_view(), name='create_opinion'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
