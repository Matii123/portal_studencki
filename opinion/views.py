"""Views file."""
from accounts.models import CustomUser
from opinion.forms import OpinionForm
from opinion.models import FieldOfStudy, Opinion
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, FormView
from django.shortcuts import redirect
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib import messages


class FieldsView(LoginRequiredMixin, TemplateView): # noqa D101
    template_name = 'opinion/fields.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs): # noqa D102
        context = super().get_context_data(**kwargs)
        fields = FieldOfStudy.objects.all()
        paginator = Paginator(fields, 5)
        page = self.request.GET.get('page')
        try:
            fields = paginator.page(page)
        except PageNotAnInteger:
            fields = paginator.page(1)
        except EmptyPage:
            fields = paginator.page(paginator.num_pages)
        context['fields'] = fields
        return context


class OpinionsView(LoginRequiredMixin, TemplateView): # noqa D101
    template_name = 'opinion/opinions.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs): # noqa D102
        context = super().get_context_data(**kwargs)
        field = FieldOfStudy.objects.get(slug=context['slug'])
        context['field'] = field
        context['opinions'] = Opinion.objects.filter(fields=field)
        return context

class CreateOpinionView(LoginRequiredMixin, FormView): # noqa D101
    template_name = 'opinion/create_opinion.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'
    model = Opinion
    form_class = OpinionForm

    def form_valid(self, form): # noqa D102
        user = CustomUser.objects.get(pk=self.request.user.pk)
        new_opinion = form.save(commit=False)
        new_opinion.user = user
        new_opinion.save()
        form.save_m2m()
        field = FieldOfStudy.objects.get(name=new_opinion.fields)
        messages.success(self.request, 'Opinia na temat kierunku została dodana.')
        return redirect('kierunek/%s' % field.slug) # noqa S001
