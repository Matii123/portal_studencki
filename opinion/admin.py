"""Admin file."""
from django.contrib import admin
from .models import Opinion, FieldOfStudy

admin.site.register(Opinion)
admin.site.register(FieldOfStudy)
