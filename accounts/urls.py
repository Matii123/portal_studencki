"""Urls file."""
from django.urls import path
from .views import RegistrationView, LoginView, LogoutUserView, Activate, \
    MyPasswordResetView, MyPasswordResetDoneView, UpdateProfileView, ProfileView
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('rejestracja/', RegistrationView.as_view(), name='register'),
    path('', LoginView.as_view(), name='login'),
    path('logout/', LogoutUserView, name='logout'),
    path('zmień_dane/', UpdateProfileView.as_view(), name='update'),
    path('profil/<slug>/', ProfileView.as_view(), name='profile'),
    path('potwierdzenie/<uidb64>/<token>', Activate, name='activate'),

    path('zresetuj_hasło/', MyPasswordResetView.as_view(), name='reset_password'),

    path('zresetuj_hasło_wysłano/', MyPasswordResetDoneView.as_view(), name='password_reset_done'),

    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(
             template_name='accounts/password_reset_form.html'),
         name='password_reset_confirm'),

    path('zrestuj_hasło_wykonane/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='accounts/password_reset_done.html'),
         name='password_reset_complete'),
]
