"""Views file."""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordResetView
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponseRedirect
from django.shortcuts import redirect

from .forms import CreateUserForm, ProfileForm
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .models import CustomUser
from .tokens import account_activation_token

from django.contrib.auth import authenticate, login, logout
from django.views.generic import TemplateView, FormView, UpdateView


class RegistrationView(FormView): # noqa D101
    template_name = 'accounts/register.html'
    model = CustomUser
    form_class = CreateUserForm

    def dispatch(self, request, *args, **kwargs): # noqa D102
        if request.user.is_authenticated:
            return HttpResponseRedirect('/informacje')
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form): # noqa D102
        email = self.request.POST.get('email')
        user = form.save()
        user.is_active = False
        user.save()
        current_site = get_current_site(self.request)
        html_content = render_to_string('accounts/active_email.html', {
            'user': user,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
        })
        text_content = render_to_string('accounts/active_email.txt', {
            'user': user,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
        })
        email_message = EmailMultiAlternatives(
            'Potwierdź adres email',
            text_content,
            'studenckiportal555@gmail.com',
            [email],
        )
        email_message.attach_alternative(html_content, 'text/html')
        email_message.send(fail_silently=False)
        messages.info(self.request, 'Potwierdź adres email, aby aktywować konto')
        return redirect('login')


class LoginView(TemplateView): # noqa D101
    template_name = 'accounts/login.html'

    def dispatch(self, request, *args, **kwargs): # noqa D102
        if request.user.is_authenticated:
            return HttpResponseRedirect('/informacje')
        return super().dispatch(request, *args, **kwargs)

    def post(self, request): # noqa D102
        email = request.POST.get('email')
        password = request.POST.get('password')

        if CustomUser.objects.filter(email=email).exists():
            username = CustomUser.objects.get(email=email).username
            verified = CustomUser.objects.get(email=email).is_active
            user = authenticate(request, username=username, password=password)
        else:
            user = None
            verified = None

        if user is not None and verified is True:
            login(request, user)
            return redirect('information')
        elif user is None and verified is True:
            messages.error(request, 'Email lub hasło jest niepoprawne.')
            return redirect('login')
        elif user is None and verified is None:
            messages.error(request, 'Nie ma takiego konta.')
            return redirect('login')
        else:
            messages.info(request, 'Konto o podanym mailu jest nieaktywne.')
            return redirect('login')


class UpdateProfileView(LoginRequiredMixin, UpdateView): # noqa D101
    template_name = 'accounts/update_profile.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'
    model = CustomUser
    form_class = ProfileForm

    def get_object(self, queryset=None): # noqa D102
        return self.request.user

    def form_valid(self, form): # noqa D102
        data = form.cleaned_data
        form.save(data)
        messages.success(self.request, 'Dane zostały zmienione.')
        return redirect('update')

    def form_invalid(self, form): # noqa D102
        messages.error(self.request, 'Podane dane są błędne.')
        return redirect('update')


class MyPasswordResetView(PasswordResetView):  # noqa D101
    template_name = 'accounts/password_reset.html'

    def dispatch(self, *args, **kwargs):  # noqa D102
        if self.request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(*args, **kwargs)


class MyPasswordResetDoneView(PasswordResetView):  # noqa D101
    template_name = 'accounts/password_reset_sent.html'

    def dispatch(self, *args, **kwargs):  # noqa D102
        if self.request.user.is_authenticated:
            return redirect('login')
        return super().dispatch(*args, **kwargs)


def LogoutUserView(request):  # noqa D103
    logout(request)
    return redirect('login')


def Activate(request, uidb64, token):  # noqa D103
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = CustomUser.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        messages.info(request, 'Konto zostało potwierdzone. Możesz się zalogować.')
        return redirect('login')
    else:
        messages.error(request, 'Niepoprawny link aktywacyjny.')
        return redirect('login')


class ProfileView(LoginRequiredMixin, TemplateView): # noqa D101
    template_name = 'accounts/profile.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs): # noqa D102
        context = super().get_context_data(**kwargs)
        slug = context['slug']
        context['user_data'] = CustomUser.objects.get(slug=slug)
        return context
