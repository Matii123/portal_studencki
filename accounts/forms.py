"""Forms file."""
from django.contrib.auth.forms import UserCreationForm
from accounts.models import CustomUser
from django.forms import ModelForm


class CreateUserForm(UserCreationForm): # noqa D101
    class Meta: # noqa D106
        model = CustomUser
        fields = ['username',
                  'email',
                  'password1',
                  'password2',
                  ]


class ProfileForm(ModelForm): # noqa D101
    class Meta: # noqa D106
        model = CustomUser
        fields = ['username',
                  'description',
                  'profile_picture',
                  ]

    def __init__(self, *args, **kwargs): # noqa D107
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class': 'form-control'})
        self.fields['description'].widget.attrs.update({'class': 'form-control'})
        self.fields['profile_picture'].widget.attrs.update({'class': 'form-control'})
