"""Models file."""
from django.contrib.auth.models import AbstractUser
from django.db import models
from django_resized import ResizedImageField
from django.utils.text import slugify
from django.shortcuts import reverse


class CustomUser(AbstractUser): # noqa D101
    email = models.EmailField(max_length=50, unique=True, blank=False, null=False)
    username = models.CharField(max_length=30, unique=True, verbose_name='username')
    slug = models.SlugField(max_length=60, unique=True, blank=True)
    description = models.TextField(max_length=200, default='brak')
    profile_picture = ResizedImageField(size=[140, 140], quality=100, upload_to='user_picture',
                                        default='user_picture/default.jpg')

    def save(self, *args, **kwargs): # noqa D102
        if not self.slug:
            self.slug = slugify(self.username)
        super(CustomUser, self).save(*args, **kwargs)

    def get_url(self): # noqa D102
        return reverse('profile', kwargs={
            'slug': self.slug,
        })
