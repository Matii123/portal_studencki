"""Models file."""
from django.db import models

class Information(models.Model): # noqa D101
    title = models.CharField(max_length=50)
    content = models.TextField()
    date = models.DateField(auto_now_add=True)

    def __str__(self): # noqa D105
        return self.title

class Event(models.Model): # noqa D101
    title = models.CharField(max_length=50)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self): # noqa D105
        return self.title
