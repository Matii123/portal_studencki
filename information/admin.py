"""Admin file."""
from django.contrib import admin

from information.models import Information, Event

admin.site.register(Information)
admin.site.register(Event)
