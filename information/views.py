"""Views file."""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from information.models import Information, Event
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


class InformationView(LoginRequiredMixin, TemplateView): # noqa D101
    template_name = 'information/information.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs): # noqa D102
        context = super().get_context_data(**kwargs)
        informations = Information.objects.all()
        paginator = Paginator(informations, 2)
        page = self.request.GET.get('page')
        try:
            informations = paginator.page(page)
        except PageNotAnInteger:
            informations = paginator.page(1)
        except EmptyPage:
            informations = paginator.page(paginator.num_pages)
        context['informations'] = informations
        context['events'] = Event.objects.order_by('start_date')
        return context
