"""Urls file."""
from django.urls import path
from .views import InformationView

urlpatterns = [
    path('informacje/', InformationView.as_view(), name='information'),
]
