# Portal internetowy dla studentów

## Opis projektu

Aplikacja została wykonana za pomocą frameworka Django i biblioteki Bootstrap. Główną funkcjonalnościa aplikacji jest możliwość zadawania pytań na forum przez studentów.

### Panel Użytkownika umożliwia

- Dodawanie oraz odpowiadanie na pytania użytkowników
- Przeglądanie głównych informacji dodawnych przez administratora strony
- Możliwość wystawienia opinii o wybranych kierunkach na studiach
- Zaktualizowanie danych użytkownika

### Panel Administracyjny umożliwia

- Dodawanie najważniejszych wydarzeń w Uniwersytecie
- Dodawanie informacji na stronie głównej aplikacji
- Możliwość edytowania danych w aplikacji

## Technologie

- Django
- Bootstrap

## Strona startowa aplikacji

![Strona startowa](main_page.png)

## Struktura bazy danych portalu

![Struktura bazy danych](structure_database_portal.png)

## Autorzy

- Mateusz Białek