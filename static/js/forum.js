function showComment(){
    var commentArea = document.getElementById("comment-area");
    var comment = document.getElementById("comment");
    commentArea.classList.remove("hide");
    comment.classList.add("hide");
}
function showReplies(id){
    var replyArea = document.getElementById(id);
    var reply_str = "reply" + id.substr(7, id.length-1);
    var reply = document.getElementById(reply_str);
    replyArea.classList.remove("hide");
    reply.classList.add("hide");
}