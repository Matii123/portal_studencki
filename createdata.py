"""Create data file."""
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')

import django

django.setup()

from opinion.models import FieldOfStudy
from forum.models import Category
from information.models import Information, Event
from django.utils.text import slugify

Fields = [
    'Administracja',
    'Analiza i kreowanie trendów',
    'Architektura krajobrazu',
    'Bezpieczeństwo narodowe',
    'Bezpieczeństwo wewnętrzne',
    'Bioinżynieria produkcji żywności',
    'Biologia',
    'Biotechnologia',
    'Budownictwo',
    'Chemia',
    'Dietetyka',
    'Dziennikarstwo',
    'Ekonomia',
    'Energetyka',
    'Filologia angielska',
    'Filozofia',
    'Geodezja',
    'Geoinformatyka',
    'Historia',
    'Informatyka',
    'Logopedia',
    'Matematyka',
    'Mechanika i budowa maszyn',
    'Mechatronika',
    'Pedagogika',
    'Pielęgniarstwo',
    'Politologia',
    'Prawo',
    'Psychologia',
    'Ratownictwo',
    'Rolnictwo',
    'Socjologia',
    'Teologia',
    'Turystyka',
    'Weterynarja',
    'Wojskoznawstwo',
    'Zarządzanie',
]

generated = False

for i in range(0, len(Fields)):
    if not FieldOfStudy.objects.filter(name=Fields[i]).exists():
        FieldOfStudy.objects.create(name=Fields[i], slug=slugify(Fields[i]))
    else:
        generated = True


Categories = [
    ['Rozrywka', 'Imprezy, integracje oraz wydarzenia sportowe odbywające się na terenie Uniwersytetu'],
    ['Kółka naukowe', 'Zajęcia pozalekcyjne dla chętnych poszerzające wiedzę z danej tematyki'],
    ['Sekcje sportowe i fakultety', 'Zajęcia sportowe m.in ze sportów zespołowych, lekkoatletyki czy tańców grupowych'],
    ['Korepetycje', 'Ogłoszenia studentów, którzy mają problemy z zaliczeniem trudnych przedmiotów'],
    ['Prace dodatkowe', 'Prace dorywcze dla chętnych studentów'],
]

for i in range(0, len(Categories)):
    if not Category.objects.filter(title=Categories[i][0]).exists():
        Category.objects.create(title=Categories[i][0], slug=slugify(Categories[i][0]), description=Categories[i][1])
    else:
        generated = True


Informations = [
    ['Portal internetowy dla studentów', 'Dziękuje za zarejestrowanie się na portalu internetowym dla studentów Uniwersytetu Warmińsko-Mazurskiego w Olsztynie. Pierwszy wpis będzie przeznaczony na opis funkcjononowania aplikacji webowej. Portal internetowy posiada wiele funkcjonalności z których najważniejszą jest forum, które przeznaczone jest dla studentów oraz ludzi, którzy są zainteresowani rozpoczęciem studiów. Studenci mogą zadawać pytania z różnych tematyk takich jak m.in. rozrywka, kółka naukowe, zajęcia sportowe, korepetycje czy prace dodatkowe. Portal składa się również z opinii absolwentów na temat różnych kierunków w uniwersytecie. Innymi funkcjonalnościami w systemie jest możliwość dodawania ogłoszeń przez administratora strony oraz dodawania do harmonogramu najważniejszych wydarzeń występujących w uniwersytecie m.in. terminów sesji, kortowiady czy dni wolnych od nauki.'],
]

for i in range(0, len(Informations)):
    if not Information.objects.filter(title=Informations[i][0]).exists():
        Information.objects.create(title=Informations[i][0], content=Informations[i][1])
    else:
        generated = True

Events = [
    ['Sesja zimowa', '2022-01-31', '2022-02-22'],
    ['Sesja letnia', '2022-06-13', '2022-09-18'],
]

for i in range(0, len(Events)):
    if not Event.objects.filter(title=Events[i][0]).exists():
        Event.objects.create(title=Events[i][0], start_date=Events[i][1], end_date=Events[i][2])
    else:
        generated = True

if generated is True:
    print('Dane zostały wygenerowane')
