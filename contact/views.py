"""Views file."""
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import ContactForm
from django.core.mail import send_mail
from django.views.generic import FormView
from django.contrib import messages


class ContactView(LoginRequiredMixin, FormView): # noqa D101
    template_name = 'contact/contact.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'
    form_class = ContactForm

    def post(self, request, *args, **kwargs): # noqa D102
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            body = {
                'email': 'Przesłane przez: ' + request.user.email,
                'message': form.cleaned_data['message'],
            }
            message = '\n'.join(body.values())
            send_mail(
                subject,
                message,
                'studenckiportal555@gmail.com',
                ['studenckiportal555@gmail.com'],
            )
            messages.success(request, 'Wiadomość została wysłana.')
            return redirect('contact')
