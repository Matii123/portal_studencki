"""Urls file."""
from django.urls import path
from .views import ContactView

urlpatterns = [
    path('kontakt/', ContactView.as_view(), name='contact'),
]
