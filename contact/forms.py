"""Forms file."""
from django import forms


class ContactForm(forms.Form): # noqa D101
    subject = forms.CharField(max_length=50)
    message = forms.CharField(widget=forms.Textarea, max_length=2000)

    def __init__(self, *args, **kwargs):  # noqa D107
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['subject'].widget.attrs.update({'class': 'form-control'})
        self.fields['message'].widget.attrs.update({'class': 'form-control'})
