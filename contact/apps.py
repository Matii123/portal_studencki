"""Apps file."""
from django.apps import AppConfig


class ContactConfig(AppConfig): # noqa D101
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'contact'
