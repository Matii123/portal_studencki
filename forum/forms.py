"""Forms file."""
from django import forms
from .models import Post


class PostForm(forms.ModelForm): # noqa D101
    class Meta: # noqa D106
        model = Post
        fields = ['title', 'content', 'categories']

    def __init__(self, *args, **kwargs): # noqa D107
        super(PostForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'class': 'form-control'})
        self.fields['content'].widget.attrs.update({'class': 'form-control'})
        self.fields['categories'].widget.attrs.update({'class': 'form-control'})
