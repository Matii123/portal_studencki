"""Views file."""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView
from django.contrib import messages

from accounts.models import CustomUser
from .models import Category, Post, Comment, Reply
from .forms import PostForm
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.utils.text import slugify


class ForumView(LoginRequiredMixin, TemplateView): # noqa D101
    template_name = 'forum/forum.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs): # noqa D102
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context


class PostView(LoginRequiredMixin, TemplateView): # noqa D101
    template_name = 'forum/detail_post.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'

    def post(self, request, slug): # noqa D102
        post = Post.objects.get(slug=slug)
        user = CustomUser.objects.get(pk=request.user.pk)

        if 'comment-form' in request.POST:
            comment = request.POST.get('comment')
            if comment != '':
                new_comment = Comment.objects.create(user=user, content=comment)
                post.comments.add(new_comment.id)
                messages.success(request, 'Komentarz został dodany.')
            else:
                messages.error(request, 'Komentarz nie może być pusty')
            return HttpResponseRedirect(reverse_lazy('post', kwargs={'slug': slug}))

        if 'reply-form' in request.POST:
            reply = request.POST.get('reply')
            comment_id = request.POST.get('comment-id')
            comment_reply = Comment.objects.get(id=comment_id)
            if reply != '':
                new_reply = Reply.objects.create(user=user, content=reply)
                comment_reply.replies.add(new_reply.id)
                messages.success(request, 'Odpowiedź na komentarz została dodana.')
            else:
                messages.error(request, 'Odpowiedź na komentarz nie może być pusta')
            return HttpResponseRedirect(reverse_lazy('post', kwargs={'slug': slug}))

    def get_context_data(self, slug, **kwargs): # noqa D102
        context = super().get_context_data(**kwargs)
        post = Post.objects.get(slug=slug)
        context['post'] = post
        return context


class PostsView(LoginRequiredMixin, TemplateView): # noqa D101
    template_name = 'forum/posts.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, slug, **kwargs): # noqa D102
        context = super().get_context_data(**kwargs)
        category = Category.objects.get(slug=slug)
        posts = Post.objects.filter(categories=category)
        paginator = Paginator(posts, 5)
        page = self.request.GET.get('page')
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.page(paginator.num_pages)
        context['posts'] = posts
        context['category'] = category
        return context


class CreatePostView(LoginRequiredMixin, FormView): # noqa D101
    template_name = 'forum/create_post.html'
    login_url = 'login'
    redirect_field_name = 'redirect_to'
    model = Post
    form_class = PostForm

    def form_valid(self, form): # noqa D102
        user = CustomUser.objects.get(pk=self.request.user.pk)
        new_post = form.save(commit=False)
        if Post.objects.filter(slug=slugify(new_post)).exists():
            messages.error(self.request, 'Już jest podobny tytuł na forum.')
            return redirect('create_post')
        else:
            new_post.user = user
            new_post.save()
            form.save_m2m()
            return redirect('post/%s' % new_post.slug) # noqa S001

    def form_invalid(self, form): # noqa D102
        messages.error(self.request, 'Nie może być taki sam tytuł na forum.')
        return redirect('create_post')
