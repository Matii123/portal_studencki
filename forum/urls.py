"""Urls file."""
from django.urls import path
from .views import ForumView, PostView, PostsView, CreatePostView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('forum/', ForumView.as_view(), name='forum'),
    path('forum/post/<slug>/', PostView.as_view(), name='post'),
    path('forum/kategoria/<slug>/', PostsView.as_view(), name='posts'),
    path('forum/dodaj_post', CreatePostView.as_view(), name='create_post'),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
