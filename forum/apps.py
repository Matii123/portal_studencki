"""Apps file."""
from django.apps import AppConfig


class ForumConfig(AppConfig): # noqa D101
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'forum'
