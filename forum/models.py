"""Models file."""
from django.db import models
from django.utils.text import slugify
from django.shortcuts import reverse
from accounts.models import CustomUser


class Category(models.Model): # noqa D101
    title = models.CharField(max_length=30, unique=True, blank=False)
    slug = models.SlugField(max_length=60, unique=True, blank=False)
    description = models.CharField(max_length=150)

    class Meta: # noqa D106
        verbose_name_plural = 'categories'

    def __str__(self): # noqa D105
        return self.title

    def get_url(self): # noqa D102
        return reverse('posts', kwargs={
            'slug': self.slug,
        })

    @property
    def num_posts(self): # noqa D102
        return Post.objects.filter(categories=self).count()

    @property
    def last_post(self): # noqa D102
        return Post.objects.filter(categories=self).latest('date')


class Reply(models.Model): # noqa D101
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, blank=True)
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    class Meta: # noqa D106
        verbose_name_plural = 'replies'

    def __str__(self): # noqa D105
        return self.content[:50]


class Comment(models.Model): # noqa D101
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, blank=True)
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    replies = models.ManyToManyField(Reply, blank=True)

    def __str__(self): # noqa D105
        return self.content[:50]


class Post(models.Model): # noqa D101
    title = models.CharField(max_length=50, unique=True, blank=False)
    slug = models.SlugField(max_length=80, unique=True, blank=True)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, blank=True)
    categories = models.ForeignKey(Category, on_delete=models.CASCADE, null=False, blank=False)
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    comments = models.ManyToManyField(Comment, blank=True)

    def __str__(self): # noqa D105
        return self.title

    def save(self, *args, **kwargs): # noqa D102
        if not self.slug:
            self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    def get_url(self): # noqa D102
        return reverse('post', kwargs={
            'slug': self.slug,
        })

    @property
    def num_comments(self): # noqa D102
        return self.comments.count()

    @property
    def last_reply(self): # noqa D102
        return self.comments.latest('date')
